variable "ALPINE_VERSION" {}
variable "DEBIAN_VERSION" {}
variable "DOCKER_VERSION" {}
variable "KANISTER_VERSION" {}
variable "KEYCLOAK_VERSION" {}
variable "KOPIA_VERSION" {}
variable "KUBECTL_VERSION" {}
variable "NODEJS_VERSION" {}
variable "OPENJDK_VERSION" {}
variable "OPENLDAP_VERSION" {}
variable "POSTGRES_VERSION" {}
variable "REGISTRY" {}
variable "TAG" {}

group "default" {
  targets = [
    "base",
    "base-debian",
    "cdn",
    "debian-build",
    "debian-nodejs",
    "docker",
    "doctl",
    "kanukopia",
    "kanukopia-openldap",
    "kanukopia-postgres",
    "keycloak",
    "keycloak-builder",
    "kube-commands",
    "kube-commands-aws",
    "kube-commands-mysql",
    "kube-commands-psql",
    "kube-commands-terraform",
    "kube-commands-terraform-aws",
    "loki",
    "nodejs",
    "nodejs-openjdk",
    "openjdk",
    "podman",
    "terraform",
  ]
}

target "base" {
  context    = "base"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/base:${TAG}",
    "${REGISTRY}/base:${ALPINE_VERSION}",
  ]
  args = {
    ALPINE_VERSION = "${ALPINE_VERSION}"
  }
}

target "base-debian" {
  context    = "base-debian"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/base-debian:${TAG}",
    "${REGISTRY}/base-debian:${DEBIAN_VERSION}",
  ]
  args = {
    DEBIAN_VERSION = "${DEBIAN_VERSION}"
  }
}

target "debian-build" {
  context    = "debian-build"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/debian-build:${TAG}",
    "${REGISTRY}/debian-build:${DEBIAN_VERSION}",
  ]
  args = {
    DEBIAN_VERSION = "${DEBIAN_VERSION}"
  }
}

target "debian-nodejs" {
  context    = "debian-nodejs"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/debian-nodejs:${TAG}",
    "${REGISTRY}/debian-nodejs:${NODEJS_VERSION}-${DEBIAN_VERSION}",
  ]
  args = {
    DEBIAN_VERSION = "${DEBIAN_VERSION}"
    NODEJS_VERSION = "${NODEJS_VERSION}"
  }
}

target "docker" {
  context    = "docker"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/docker:${TAG}",
    "${REGISTRY}/docker:${DOCKER_VERSION}",
  ]
  contexts = {
    base = "target:base"
  }
}

target "kanukopia" {
  context    = "kanukopia"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kanukopia:${TAG}",
    "${REGISTRY}/kanukopia:${KANISTER_VERSION}",
    "${REGISTRY}/kanukopia:${KANISTER_VERSION}-${DEBIAN_VERSION}",
  ]
  args = {
    DEBIAN_VERSION   = "${DEBIAN_VERSION}"
    KANISTER_VERSION = "${KANISTER_VERSION}"
    KOPIA_VERSION    = "${KOPIA_VERSION}"
    KUBECTL_VERSION  = "${KUBECTL_VERSION}"
  }
}

target "kanukopia-openldap" {
  context    = "kanukopia-openldap"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kanukopia-openldap:${TAG}",
    "${REGISTRY}/kanukopia-openldap:${OPENLDAP_VERSION}",
  ]
  args = {
    OPENLDAP_VERSION = "${OPENLDAP_VERSION}"
  }
}

target "kanukopia-postgres" {
  context    = "kanukopia-postgres"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kanukopia-postgres:${TAG}",
    "${REGISTRY}/kanukopia-postgres:${KANISTER_VERSION}",
    "${REGISTRY}/kanukopia-postgres:${KANISTER_VERSION}-${DEBIAN_VERSION}",
  ]
  contexts = {
    kanukopia = "target:kanukopia"
  }
}

target "keycloak-builder" {
  context    = "keycloak-builder"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  tags = [
    "${REGISTRY}/keycloak-builder:${TAG}",
    "${REGISTRY}/keycloak-builder:${KEYCLOAK_VERSION}",
  ]
  args = {
    KEYCLOAK_VERSION = "${KEYCLOAK_VERSION}"
  }
}

target "keycloak" {
  context    = "keycloak"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  tags = [
    "${REGISTRY}/keycloak:${TAG}",
    "${REGISTRY}/keycloak:${KEYCLOAK_VERSION}",
  ]
  args = {
    KEYCLOAK_VERSION = "${KEYCLOAK_VERSION}"
  }
  contexts = {
    keycloak-builder = "target:keycloak-builder"
  }
}

target "kube-commands" {
  context    = "kube-commands"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands:${TAG}",
    "${REGISTRY}/kube-commands:${ALPINE_VERSION}",
  ]
  args = {
    ALPINE_VERSION = "${ALPINE_VERSION}"
  }
  contexts = {
    base = "target:base"
  }
}

target "kube-commands-aws" {
  context    = "kube-commands-aws"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands-aws:${TAG}",
    "${REGISTRY}/kube-commands-aws:${ALPINE_VERSION}",
  ]
  contexts = {
    kube-commands = "target:kube-commands"
  }
}

target "kube-commands-mysql" {
  context    = "kube-commands-mysql"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands-mysql:${TAG}",
    "${REGISTRY}/kube-commands-mysql:${ALPINE_VERSION}",
  ]
  contexts = {
    kube-commands = "target:kube-commands"
  }
}

target "kube-commands-psql" {
  context    = "kube-commands-psql"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands-psql:${TAG}",
    "${REGISTRY}/kube-commands-psql:${ALPINE_VERSION}",
  ]
  contexts = {
    kube-commands = "target:kube-commands"
  }
}

target "kube-commands-terraform" {
  context    = "kube-commands-terraform"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands-terraform:${TAG}",
    "${REGISTRY}/kube-commands-terraform:${ALPINE_VERSION}",
  ]
  contexts = {
    kube-commands = "target:kube-commands"
  }
}

target "kube-commands-terraform-aws" {
  context    = "kube-commands-terraform-aws"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/kube-commands-terraform-aws:${TAG}",
    "${REGISTRY}/kube-commands-terraform-aws:${ALPINE_VERSION}",
  ]
  contexts = {
    kube-commands-aws = "target:kube-commands-aws"
  }
}

target "loki" {
  context    = "loki"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/loki:${TAG}",
    "${REGISTRY}/loki:${NODEJS_VERSION}-${DEBIAN_VERSION}",
  ]
  contexts = {
    debian-nodejs = "target:debian-nodejs"
  }
}

target "nodejs" {
  context    = "nodejs"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/nodejs:${TAG}",
    "${REGISTRY}/nodejs:${NODEJS_VERSION}-${ALPINE_VERSION}",
  ]
  args = {
    ALPINE_VERSION = "${ALPINE_VERSION}"
    NODEJS_VERSION = "${NODEJS_VERSION}"
  }
}

target "openjdk" {
  context    = "openjdk"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/openjdk:${TAG}",
    "${REGISTRY}/openjdk:${OPENJDK_VERSION}-${DEBIAN_VERSION}",
    "${REGISTRY}/openjdk:${OPENJDK_VERSION}",
  ]
  contexts = {
    base-debian = "target:base-debian"
  }
  args = {
    OPENJDK_VERSION = "${OPENJDK_VERSION}"
  }
}

target "podman" {
  context    = "podman"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/podman:${TAG}",
    "${REGISTRY}/podman:${ALPINE_VERSION}",
  ]
  contexts = {
    base = "target:base"
  }
}

target "terraform" {
  context    = "terraform"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/terraform:${TAG}",
    "${REGISTRY}/terraform:${ALPINE_VERSION}",
  ]
  contexts = {
    base = "target:base"
  }
}

target "nodejs-openjdk" {
  context    = "nodejs-openjdk"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/nodejs-openjdk:${TAG}",
    "${REGISTRY}/nodejs-openjdk:${OPENJDK_VERSION}-${NODEJS_VERSION}-${DEBIAN_VERSION}",
    "${REGISTRY}/nodejs-openjdk:${OPENJDK_VERSION}-${NODEJS_VERSION}",
  ]
  args = {
    OPENJDK_VERSION = "${OPENJDK_VERSION}"
  }
  contexts = {
    debian-nodejs = "target:debian-nodejs"
  }
}

target "doctl" {
  context    = "doctl"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/doctl:${TAG}"
  ]
}

target "frappe-kanister-devcontainer" {
  context    = "frappe-kanister-devcontainer"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/frappe-kanister-devcontainer:${TAG}"
  ]
  args = {
    KANISTER_VERSION = "${KANISTER_VERSION}"
    KOPIA_VERSION    = "${KOPIA_VERSION}"
    KUBECTL_VERSION  = "${KUBECTL_VERSION}"
  }
}

target "cdn" {
  context    = "cdn"
  dockerfile = "Dockerfile"
  platforms  = [
    "linux/amd64",
    # "linux/arm64",
  ]
  tags = [
    "${REGISTRY}/cdn:${TAG}",
    "${REGISTRY}/cdn:${NODEJS_VERSION}-${ALPINE_VERSION}",
  ]
  args = {
    ALPINE_VERSION = "${ALPINE_VERSION}"
    NODEJS_VERSION = "${NODEJS_VERSION}"
  }
}
